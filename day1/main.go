package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	c, _ := ioutil.ReadFile("input.txt")

	vals := strings.Split(string(c), "\n")

	r := []int{}
	regP := regexp.MustCompile("^\\+([0-9]+)")
	regN := regexp.MustCompile("^\\-([0-9]+)")
	for _, v := range vals {
		m := regP.FindStringSubmatch(v)
		if len(m) > 1 {
			q, _ := strconv.Atoi(m[1])
			r = append(r, q)
		} else if vv := regN.FindStringSubmatch(v); len(vv) > 1 {
			q, _ := strconv.Atoi(vv[1])
			r = append(r, q*-1)
		}
	}

	m := make(map[int]bool)
	found := false

	t := 0
	for !found {
		for _, v := range r {
			t += v
			if m[t] == true {
				fmt.Printf("Found %d\n", t)
				found = true
				return
			}
			m[t] = true
		}
	}
}
